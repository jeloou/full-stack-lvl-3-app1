var express = require('express');
var app = express();

app.set('view engine', 'jade');
app.set('views', 'app');

app.use(express.static('public'));

app.get('/', function(req, res) {
  res.render('index', {
    title: 'Express.js Todos'
  });
});

module.exports = app;
